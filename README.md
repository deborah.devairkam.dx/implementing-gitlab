# Partner Technical Bootcamp: Implementing GitLab

* The [complete bootcamp](https://about.gitlab.com/handbook/resellers/bootcamp/index.html) is composed of two different sections: "Using GitLab" and "Implementing GitLab".
* This project includes all labs to be used in the "Implementing GitLab" Bootcamp
* Labs take advantage of paid tier features; to get the full experience your instance of GitLab, or your Group if using gitlab.com, should be licensed to Ultimate/Gold. 
  * [Get a trial here](https://about.gitlab.com/free-trial/). 
* You will require access to a cloud hosting provider such as Amazon Web Services, Google Cloud Platform or similar, in order to provision virtual machines and kubernetes clusters as part of the lab excercises.
* Lab exercises are defined as individual issues within this project
* Import the latest exported version of this project which can be found [here](https://drive.google.com/file/d/14lSlwEweMahLSiXDFxmLDJKep-HS3L5V/view?usp=sharing)
